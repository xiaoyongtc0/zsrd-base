/*
SQLyog Ultimate v12.5.0 (64 bit)
MySQL - 8.0.30 : Database - zsrd
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`zsrd` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `zsrd`;

/*Table structure for table `sys_dept` */

DROP TABLE IF EXISTS `sys_dept`;

CREATE TABLE `sys_dept` (
  `dept_id` bigint NOT NULL COMMENT '部门id',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  `parent_dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `order_num` int NOT NULL DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `mpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `create_time` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建时间',
  `update_time` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6) COMMENT '更新时间',
  `del_flag` int NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_dept` */

insert  into `sys_dept`(`dept_id`,`dept_name`,`parent_dept_id`,`order_num`,`leader`,`phone`,`mpath`,`status`,`create_time`,`update_time`,`del_flag`) values 
(1,'总部',0,1,'admin','110','','0','2023-11-10 11:23:18.169584','2023-12-04 16:21:49.026386',0),
(2,'研发部',1,1,'妍妍',NULL,'','0','2023-12-01 11:07:15.737000','2023-12-04 16:22:05.626542',0),
(3,'销售部',1,2,'肖肖',NULL,'','0','2023-12-01 10:11:57.164000','2023-12-04 16:22:14.040801',0),
(4,'研发一组',2,1,'法益',NULL,'','0','2023-12-01 10:12:04.948000','2023-12-04 16:22:30.507140',0),
(5,'研发二组',2,2,'法尔',NULL,'','0','2023-12-01 10:17:20.074000','2023-12-04 16:23:37.989388',0),
(6,'市场部',9,3,NULL,NULL,'','0','2023-12-01 15:29:29.019214','2023-12-04 16:22:45.234225',0),
(7,'技术部',2,0,NULL,NULL,'','0','2023-12-01 16:27:50.644548','2023-12-04 16:23:52.076700',0),
(8,'实施组',7,0,NULL,NULL,'','0','2023-12-01 16:28:03.550426','2023-12-04 16:24:03.845933',0),
(9,'董事会',0,0,NULL,NULL,'','0','2023-12-01 16:49:39.788966','2023-12-04 16:24:10.190600',0),
(10,'测试组',9,0,NULL,NULL,'','0','2023-12-04 14:41:51.000000','2023-12-04 16:24:18.203716',0);

/*Table structure for table `sys_dict` */

DROP TABLE IF EXISTS `sys_dict`;

CREATE TABLE `sys_dict` (
  `id` bigint NOT NULL,
  `code_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
  `code_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名',
  `code_kind` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`,`code_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_dict` */

insert  into `sys_dict`(`id`,`code_value`,`code_name`,`code_kind`) values 
(1,'10','男','gender'),
(2,'20','女','gender');

/*Table structure for table `sys_menu` */

DROP TABLE IF EXISTS `sys_menu`;

CREATE TABLE `sys_menu` (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `parent_menu_id` bigint DEFAULT NULL COMMENT '菜单id',
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '路由别名',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '路由地址',
  `order_num` int NOT NULL COMMENT '显示顺序',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '组件路径',
  `mpath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '',
  `query` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '路由参数',
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '外链地址',
  `icon` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '#' COMMENT '菜单图标',
  `is_frame` int NOT NULL DEFAULT '0' COMMENT '是否为外链（0否 1是）',
  `is_keep_alive` int NOT NULL DEFAULT '0' COMMENT '是否缓存（0否 1是）',
  `is_full` int NOT NULL DEFAULT '0' COMMENT '是否全屏（0否 1是）',
  `is_hide` int NOT NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `create_time` datetime(6) NOT NULL COMMENT '创建时间',
  `update_time` datetime(6) NOT NULL COMMENT '更新时间',
  `del_flag` int DEFAULT NULL COMMENT '是否删除',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_menu` */

insert  into `sys_menu`(`menu_id`,`parent_menu_id`,`name`,`menu_name`,`path`,`order_num`,`component`,`mpath`,`query`,`link_url`,`icon`,`is_frame`,`is_keep_alive`,`is_full`,`is_hide`,`create_time`,`update_time`,`del_flag`) values 
(1,NULL,'home','首页','/home/index',1,'/test/home/index','1.',NULL,NULL,'HomeFilled',0,1,0,0,'2023-05-18 08:47:55.374506','2023-05-18 08:47:55.397354',NULL),
(2,NULL,'dataScreen','数据大屏','/dataScreen',20,'/test/dataScreen/index','2.',NULL,NULL,'Histogram',0,1,1,0,'2023-05-18 08:47:55.374506','2023-05-18 08:47:55.397354',NULL),
(3,NULL,'proTable','超级表格','/test/proTable',30,NULL,'3.',NULL,NULL,'MessageBox',0,0,0,0,'2023-05-18 08:47:55.374506','2023-05-30 10:14:25.000000',NULL),
(4,3,'useProTable','使用 ProTable','/test/proTable/useProTable',1,'/test/proTable/useProTable/index','3.4.',NULL,NULL,'Menu',0,0,0,0,'2023-05-18 08:47:55.374506','2023-05-30 10:14:25.000000',NULL),
(5,NULL,'system','系统管理','/system',4,NULL,'4.',NULL,NULL,'Tools',0,0,0,0,'2023-05-18 08:47:55.374506','2023-05-18 08:47:55.397354',NULL),
(6,5,'menuManage','菜单管理','/system/menuManage',1,'/test/system/menuManage/index','5.6.',NULL,NULL,'Menu',0,0,0,0,'2023-05-18 08:47:55.374506','2023-11-13 10:34:59.727704',NULL),
(7,NULL,'authority','权限管理','/authority',1,NULL,'',NULL,NULL,'Menu',0,0,0,0,'2023-12-08 09:19:47.000000','2023-12-08 09:19:54.000000',NULL),
(16,7,'roleManage','角色管理','/authority/roleManage',2,'/test/system/roleManage/index','4.16.',NULL,NULL,'Menu',0,0,0,0,'2023-05-29 13:52:34.862559','2023-05-29 13:53:13.188821',NULL),
(17,NULL,'assembly','常用组件','/assembly',5,NULL,'17.',NULL,NULL,'Briefcase',0,0,0,0,'2023-05-30 09:51:20.083832','2023-05-30 09:51:20.000000',NULL),
(18,17,'treeFilter','树形选择器','/assembly/treeFilter',2,'/test/assembly/treeFilter/index','17.18.',NULL,NULL,'Menu',0,0,0,0,'2023-05-30 09:52:48.493858','2023-05-30 09:57:18.861322',NULL),
(19,17,'authButton','按钮权限','/assembly/authButton',1,'/test/assembly/authButton/index','17.19.',NULL,NULL,'Menu',0,0,0,0,'2023-05-30 09:54:16.844412','2023-05-30 09:57:26.123931',NULL),
(20,17,'selectFilter','分类筛选器','/assembly/selectFilter',3,'/test/assembly/selectFilter/index','17.20.',NULL,NULL,'Menu',0,0,0,0,'2023-06-02 13:56:58.536804','2023-06-02 13:58:35.000000',NULL),
(21,5,'deptManage','部门管理','/system/deptManage',3,'/test/system/deptManage/index','4.21.',NULL,NULL,'Menu',0,0,0,0,'2023-11-10 09:30:03.160998','2023-11-10 09:31:23.820820',NULL),
(22,5,'userManage','用户管理','/system/userManage',4,'/test/system/userManage/index','4.22.',NULL,NULL,'Menu',0,0,0,0,'2023-11-13 10:49:13.914505','2023-11-17 14:57:20.000000',NULL),
(23,NULL,'directives','自定义指令','/directives',6,NULL,'23.',NULL,NULL,'UserFilled',0,0,0,0,'2023-11-17 13:46:31.202904','2023-11-17 13:50:14.436967',NULL),
(24,23,'copyDirect','复制指令','/directives/copyDirect',1,'/test/directives/copyDirect/index','23.24.',NULL,NULL,'Menu',0,0,0,0,'2023-11-17 15:02:43.043574','2023-11-17 15:02:57.000000',NULL),
(25,23,'watermarkDirect','水印指令','/directives/watermarkDirect',2,'/test/directives/watermarkDirect/index','23.25.',NULL,NULL,'Menu',0,0,0,0,'2023-11-17 15:19:49.072809','2023-11-17 15:19:59.000000',NULL),
(26,23,'debounceDirect','防抖指令','/directives/debounceDirect',3,'/test/directives/debounceDirect/index','23.26.',NULL,NULL,'Menu',0,0,0,0,'2023-11-17 15:21:01.884034','2023-11-20 15:01:34.000000',NULL),
(27,23,'throttleDirect','节流指令','/directives/throttleDirect',4,'/test/directives/throttleDirect/index','23.27.',NULL,NULL,'Menu',0,0,0,0,'2023-11-17 15:22:03.005268','2023-11-20 15:55:48.000000',NULL),
(28,23,'longpressDirect','长按指令','/directives/longpressDirect',5,'/test/directives/longpressDirect/index','23.28.',NULL,NULL,'Menu',0,0,0,0,'2023-11-17 15:22:59.337500','2023-11-17 15:22:59.000000',NULL);

/*Table structure for table `sys_oper_log` */

DROP TABLE IF EXISTS `sys_oper_log`;

CREATE TABLE `sys_oper_log` (
  `oper_id` bigint NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  `function_type` varchar(255) DEFAULT NULL COMMENT '模块所属功能',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='操作日志记录';

/*Data for the table `sys_oper_log` */

insert  into `sys_oper_log`(`oper_id`,`title`,`business_type`,`method`,`request_method`,`operator_type`,`oper_name`,`dept_name`,`oper_url`,`oper_ip`,`oper_location`,`oper_param`,`json_result`,`status`,`error_msg`,`oper_time`,`function_type`) values 
(12,'修改角色对应的菜单',2,'com.zsrd.system.controller.RoleController.updateMenu()','POST',1,'admin','','/system/roleManage/updateMenu','127.0.0.1','','{\"roleId\":1,\"menuId\":\"1,2,3,55\"}','',1,'/ by zero','2023-12-06 15:24:04','MENUMANAGE'),
(13,'修改角色对应的菜单',2,'com.zsrd.system.controller.RoleController.updateMenu()','POST',1,'admin','','/system/roleManage/updateMenu','127.0.0.1','','{\"roleId\":1,\"menuId\":\"1,2,3,55\",\"updateTime\":1701847509327}','{\"code\":\"10000\",\"currentPage\":0,\"currentResult\":0,\"entityOrField\":false,\"message\":\"成功\",\"showCount\":10,\"success\":true,\"totalPage\":0,\"totalResult\":0}',0,'','2023-12-06 15:25:10','MENUMANAGE'),
(19,'修改角色对应的菜单',2,'com.zsrd.system.controller.RoleController.updateMenu()','POST',1,'admin','','/system/roleManage/updateMenu','192.168.31.206','','{\"createTime\":1683359432000,\"roleId\":1,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"menuId\":\"1,2,3,4,5,6,16,21,22,7\",\"updateTime\":1701999514989,\"delFlag\":\"0\"}','{\"code\":\"10000\",\"currentPage\":0,\"currentResult\":0,\"entityOrField\":false,\"message\":\"成功\",\"showCount\":10,\"success\":true,\"totalPage\":0,\"totalResult\":0}',0,'','2023-12-08 09:38:35','MENUMANAGE');

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_role` */

insert  into `sys_role`(`role_id`,`role_name`,`role_key`,`create_time`,`update_time`,`del_flag`) values 
(1,'超级管理员','admin','2023-05-06 15:50:32','2023-12-08 09:38:35','0'),
(2,'管理员','manager','2023-09-22 15:46:26','2023-12-04 16:32:39','0'),
(3,'运营','operate','2023-05-06 07:50:32','2023-05-06 07:50:32','0'),
(4,'测试','test','2023-05-06 07:50:32','2023-05-06 07:50:32','0');

/*Table structure for table `sys_role_dept` */

DROP TABLE IF EXISTS `sys_role_dept`;

CREATE TABLE `sys_role_dept` (
  `role_id` bigint NOT NULL,
  `dept_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_role_dept` */

insert  into `sys_role_dept`(`role_id`,`dept_id`) values 
(1,1),
(2,9);

/*Table structure for table `sys_role_menu` */

DROP TABLE IF EXISTS `sys_role_menu`;

CREATE TABLE `sys_role_menu` (
  `role_id` bigint NOT NULL,
  `menu_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_role_menu` */

insert  into `sys_role_menu`(`role_id`,`menu_id`) values 
(2,1),
(2,5),
(2,6),
(2,16),
(2,21),
(2,22),
(1,1),
(1,2),
(1,3),
(1,4),
(1,5),
(1,6),
(1,16),
(1,21),
(1,22),
(1,7);

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户昵称',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `status` int DEFAULT NULL COMMENT '状态',
  `gender` varchar(20) COLLATE utf8mb4_general_ci DEFAULT '10' COMMENT '性别',
  `salt` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '盐加密',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `del_flag` int DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123456801 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_user` */

insert  into `sys_user`(`user_id`,`username`,`password`,`nick_name`,`dept_id`,`status`,`gender`,`salt`,`remark`,`create_time`,`update_time`,`del_flag`) values 
(1,'admin','$2a$10$iQMdDV3UI26DReHkoAaGv.t8IjYXc6DSdtS1Dd2NCGmslz5hHI3Tq','超级管理员',0,1,'10','','访客','2023-11-30 09:14:43','2023-11-30 09:14:46',0),
(2,'manager','$2a$10$rZ4fH3/re1tsuvX4Ezf1S.KePh1KGhwkDfVmW0cFsI4Iqa8oazVd2','管理员',0,1,'10','','访客','2023-11-30 09:15:51','2023-11-30 09:15:53',0);

/*Table structure for table `sys_user_role` */

DROP TABLE IF EXISTS `sys_user_role`;

CREATE TABLE `sys_user_role` (
  `user_id` bigint NOT NULL,
  `role_id` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `sys_user_role` */

insert  into `sys_user_role`(`user_id`,`role_id`) values 
(2,2),
(1,1),
(1,2),
(1,3),
(1,4);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
