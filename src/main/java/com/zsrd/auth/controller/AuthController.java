package com.zsrd.auth.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author  LY
 * @ClassName AuthController
 * @Date 2023/6/10 14:33
 * @Description
 * @Version 1.0
 */
@RestController
public class AuthController {

    @PreAuthorize("hasAuthority('admin')")
    @RequestMapping(value = "/getStr1",method = {RequestMethod.GET})
    public String getStr1(){
        return "getStr1";
    }

    @PreAuthorize("hasAuthority('test')")
    @RequestMapping(value = "/getStr2",method = {RequestMethod.GET})
    public String getStr2(){
        return "getStr2";
    }

    @RequestMapping(value = "/getStr3",method = {RequestMethod.GET})
    public String getStr3(){
        return "getStr3";
    }
}
