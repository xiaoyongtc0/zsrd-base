package com.zsrd.auth.controller;

import com.zsrd.auth.core.ResponseSupport;
import com.zsrd.auth.core.UserInfoUtil;
import com.zsrd.user.domain.SysUser;
import com.zsrd.user.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SysUserService sysUserService;

    // 登录接口，什么也不传，通过token获取用户信息
    @GetMapping("/userinfo")
    public ResponseSupport userinfo() {
        Map<String, Object> userInfo =UserInfoUtil.userinfo();
        return ResponseSupport.success(userInfo);
    }


//    /**
//     * 注册接口
//     * 通过前端传过来的账号密码，新生成一个token
//     * @return
//     */
//    @PostMapping("register")
//    public Object register( @RequestBody SysUser sysUser) {
//        String pass = sysUser.getPassword();
//        String hashPass = passwordEncoder.encode(pass);
//        sysUser.setPassword(hashPass);
//        return sysUserService.save(sysUser);
//    }
}
