package com.zsrd.auth.domain;

import com.zsrd.user.domain.SysUser;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Date;

/**
 * @Author LY
 * @ClassName SecurityUser
 * @Date 2023/6/10 14:58
 * @Description Security用户信息
 * @Version 1.0
 */
@Data
public class SecurityUser implements UserDetails {

    private static final long serialVersionUID = 4027659018040595544L;



    private Long userId;
    private String username;
    private String password;
    private String nickname;
    private long deptid;
    private Boolean status;
    private String gender;
    private String salt;
    private String remark;
    private Date  createtime;
    private Date  updatetime;
    private Integer delFlag;



    private Collection<SimpleGrantedAuthority> authorities;

    public SecurityUser(SysUser sysUser) {
        this.setUserId(sysUser.getUserId());
        this.setDelFlag(sysUser.getDelFlag());
        this.setUsername(sysUser.getUsername());
        this.setPassword(sysUser.getPassword());
        this.setStatus(sysUser.getStatus() == 1);
        this.setNickname(sysUser.getNickName());
        this.setDeptid(sysUser.getDeptId());
        this.setGender(sysUser.getGender());
        this.setSalt(sysUser.getSalt());
        this.setRemark(sysUser.getRemark());
        this.setCreatetime(sysUser.getCreateTime());
        this.setUpdatetime(sysUser.getUpdateTime());
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.status;
    }
}
