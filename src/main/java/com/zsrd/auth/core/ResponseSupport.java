package com.zsrd.auth.core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <pre>
 *
 * </pre>
 *
 * @author sunjiyong
 * @since 2023/9/19 16:22
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public  class ResponseSupport<T> extends Page {


    /**
     * 响应参数支持
     *
     * @author WANGHT ON 2019年11月21日 22:40:26
     *
     */


    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 静态创建响应对象
     *
     * @param code
     * @param message
     * @param isSuccess
     * @return
     */
    public static <T> ResponseSupport<T> of(String code, String message, boolean isSuccess) {
        return new ResponseSupport<>(code, message, isSuccess);
    }

    /**
     * 静态创建响应对象
     *
     * @param code
     * @param message
     * @param exception
     * @return
     */
    public static <T> ResponseSupport<T> of(String code, String message, Exception exception) {
        return new ResponseSupport<>(code, message, exception);
    }

    /**
     * 静态创建响应对象
     *
     * @param code
     * @param message
     * @param isSuccess
     * @return
     */
    public static <T> ResponseSupport<T> of(String code, String message, boolean isSuccess, T t) {
        return new ResponseSupport<>(code, message, isSuccess, t);
    }

    /**
     * 失败
     *
     * @return
     */
    public static <T> ResponseSupport<T> fail() {
        return (ResponseSupport<T>) fail(BaseEnum.ResultEnum.Fail.getName());
    }

    /**
     * 失败
     *
     * @param message
     * @return
     */
    public static <T> ResponseSupport<Object> fail(String message) {
        return of(BaseEnum.ResultEnum.Fail.getCode() + "", message, false);
    }

    /**
     * 失败
     *
     * @param
     * @return
     */
    public static <T> ResponseSupport<T> fail(Integer code) {
        return of(code.toString(), BaseEnum.ResultEnum.getNameByCode(code), false);
    }

    /**
     * 失败
     *
     * @param message
     * @param exception
     *
     * @return
     */
    public static <T> ResponseSupport<Object> fail(String message, Exception exception) {
        return of(BaseEnum.ResultEnum.Fail.getCode() + "", message, exception);
    }

    /**
     * 成功
     *
     * @return
     */
    public static <T> ResponseSupport<T> success() {
        return success(BaseEnum.ResultEnum.Success.getName());
    }

    /**
     * 成功
     *
     * @param message
     * @return
     */
    public static <T> ResponseSupport<T> success(String message) {
        return of(BaseEnum.ResultEnum.Success.getCode() + "", message, true);
    }

    /**
     * 成功
     *
     * @return
     */
    public static <T> ResponseSupport<T> success(T t) {

        return success(BaseEnum.ResultEnum.Success.getName(), t);
    }

    /**
     * 成功
     *
     * @param message
     * @return
     */
    public static <T> ResponseSupport<T> success(String message, T t) {
        return of(BaseEnum.ResultEnum.Success.getCode() + "", message, true, t);
    }

    /**
     * 无返回结果构造器
     *
     * @param code
     * @param message
     * @param isSuccess
     */
    public ResponseSupport(String code, String message, boolean isSuccess) {
        this.code = code;
        this.message = message;
        this.isSuccess = isSuccess;
    }

    /**
     * 无返回结果构造器
     *
     * @param code
     * @param message
     * @param exception
     */
    public ResponseSupport(String code, String message, Exception exception) {
        this.code = code;
        this.message = message;
        this.exception = exception;
    }

    /**
     * 带返回结果的构造器
     *
     * @param code
     * @param message
     * @param isSuccess
     * @param result
     */
    public ResponseSupport(String code, String message, boolean isSuccess, T result) {
        this.code = code;
        this.message = message;
        this.isSuccess = isSuccess;
        this.result = result;
    }

    /** 请求是否成功 */
    private boolean isSuccess;

    /** 响应状态码 */
    protected String code;

    /** 响应状信息 */
    protected String message;

    /** 响应数据 */
    protected T result;

    /** 异常 */
    private Exception exception;

    private String flatId;

    private String corpId;
}

