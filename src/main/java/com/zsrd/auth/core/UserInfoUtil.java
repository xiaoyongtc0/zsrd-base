package com.zsrd.auth.core;

import com.zsrd.user.mapper.SysUserMapper;
import com.zsrd.user.service.SysUserService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
public class UserInfoUtil {

    // 登录接口，什么也不传，通过token获取用户信息
    public static   Map<String, Object> userinfo() {
        String userId;
        Map<String, Object> userInfo = new HashMap<>();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof OAuth2Authentication) {
            OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
            Object details = oAuth2Authentication.getUserAuthentication().getPrincipal();
            if (details == null) {
                throw new RuntimeException("获取用户信息失败！");
            } else {
                //获取用户详细信息
                userInfo = object2Map(details);
                userId = userInfo == null ? null : String.valueOf(userInfo.get("user_id"));
            }
        }
        return userInfo;
    }

    public static String getLocalUser(String remoteUsername){
        String u = "";
        // 模拟通过三方用户查本地用户
        if(StringUtils.isNotEmpty(remoteUsername)){
            u = "local_admin";
        }
        return u;
}


    /**
     * 根据当前用户获取绑定的角色
     * @return
     */
    private static List<String> getUserRole(){

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
    List<String> collect = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
//        for (GrantedAuthority authority : authorities) {
//        String authority1 = authority.getAuthority();
//        list.add(authority1);
//    }
    return collect;
}



    /**
     * 实体对象转成Map
     *
     * @param obj 实体对象
     * @return
     */
    public static Map<String, Object> object2Map(Object obj) {
        Map<String, Object> map = new HashMap<>();
        if (obj == null) {
            return map;
        }
        Class clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                map.put(field.getName(), field.get(obj));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
}
