package com.zsrd.auth.core;


import lombok.AllArgsConstructor;
import lombok.Getter;

public class BaseEnum {
    public enum ResultEnum {
        Success("成功", 10000),
        Fail("失败", -9999),
        ValidateFail("验证失败", -9998),
        SecuritySqlFail("SQL注入问题", -9997),
        SecurityCharFail("特殊字符问题", -9996),
        NoPermission("该请求无权限", -9995),
        NoGreenToken("绿色国网TOKEN不正确", -8990),
        NoGreenPublicCode("绿色国网publicCode不存在", -8989),
        NoLogin("请登录", -9994),
        LoginExpired("登录时间过期", -9993),
        RepeatAttack("存在重放攻击", -9992),
        SingleLogin("本账号已在其它地方登录", -9991),
        NoToken("无token", -9990),
        TokenError("Token错误", -8999),
        SecurityXssFail("XSS注入问题", -8995),
        UsernameError("用户名或密码错误", -9980),
        ValidateCodeExpire("验证码过期", 20001),
        ValidateCodeError("验证码错误", 20002),
        ServiceProblems("服务存在问题", 30000),
        DecryptionSM2Error("请求数据解密错误", -8994),
        EncryptionSM2Error("响应数据加密错误", -8991),
        SM3DigestError("验签失败", -8993),
        RequestBodyError("请求数据异常", -8992),
        ServerError("服务连接中断, 请稍后再试", -9000);

        // 成员变量
        private String name;
        private int code;

        // 构造方法
        private ResultEnum(String name, int code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getNameByCode(int code) {
            for (ResultEnum c : ResultEnum.values()) {
                if (code == c.getCode()) {
                    return c.name;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public int getCode() {
            return code;
        }
    }

    /***
     * 省份枚举各身份证对应的编号
     * 1、华北地区：北京市|11，天津市|12，河北省|13，山西省|14，内蒙古自治区|15；
     * 2、东北地区： 辽宁省|21，吉林省|22，黑龙江省|23；
     * 3、华东地区： 上海市|31，江苏省|32，浙江省|33，安徽省|34，福建省|35，江西省|36，山东省|37；
     * 4、华中地区： 河南省|41，湖北省|42，湖南省|43；
     * 5、华南地区：广东省|44，广西壮族自治区|45，海南省|46；
     * 6、西南地区： 四川省|51，贵州省|52，云南省|53，西藏自治区|54，重庆市|50；
     * 7、西北地区： 陕西省|61，甘肃省|62，青海省|63，宁夏回族自治区|64，新疆维吾尔自治区|65；
     * 8、特别地区：台湾地区(886)|83，香港特别行政区（852)|81，澳门特别行政区（853)|82。
     */
    public enum ProvinceEnum {
        //华北地区
        BeiJing("北京市", "11"),
        TianJin("天津市", "12"),
        HeiBei("河北省", "13"),
        ShanXi("山西省", "14"),
        NeiMengGu("内蒙古自治区", "15"),
        //东北地区
        LiaoNing("辽宁省", "21"),
        JiLin("吉林省", "22"),
        HeiLongJiang("黑龙江省", "23"),
        //华东地区
        ShangHai("上海市", "31"),
        JiangSu("江苏省", "32"),
        ZheJiang("浙江省", "33"),
        AnHui("安徽省", "34"),
        FuJian("福建省", "35"),
        JiangXi("江西省", "36"),
        ShanDong("山东省", "37"),
        //华中地区
        HeNan("河南省", "41"),
        HuBei("湖北省", "42"),
        HuNan("湖南省", "43"),
        //华南地区
        GuangDong("广东省", "44"),
        GuangXi("广西壮族自治区", "45"),
        HaiNan("海南省", "46"),
        //西南地区
        SiChuan("四川省", "51"),
        GuiZhou("贵州省", "52"),
        YunNan("云南省", "53"),
        XiZang("西藏自治区", "54"),
        ChongQing("重庆市", "50"),

        //西北地区
        ShaanXi("陕西省", "61"),
        GanShu("甘肃省", "62"),
        QingHai("青海省", "63"),
        NingXia("宁夏回族自治区", "64"),
        XinJiang("新疆维吾尔自治区", "65");

        // 成员变量
        private String name;
        private String code;

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        // 构造方法
        private ProvinceEnum(String name, String code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getNameByCode(String code) {
            for (ProvinceEnum c : ProvinceEnum.values()) {
                if (code.equals(c.getCode())) {
                    return c.name;
                }
            }
            return null;
        }
    }

    public enum InstanceTypeEnum {
        JSON_INTERFACE("json接口", 1),
        WEBSERVICE_INTERFACE("webservice接口", 0);
        // 成员变量
        private String name;
        private Integer code;

        public String getName() {
            return name;
        }

        public Integer getCode() {
            return code;
        }

        // 构造方法
        private InstanceTypeEnum(String name, Integer code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getNameByCode(Integer code) {
            for (InstanceTypeEnum c : InstanceTypeEnum.values()) {
                if (code.equals(c.getCode())) {
                    return c.name;
                }
            }
            return null;
        }
    }

    /***
     * 任务的枚举
     */
    public enum TaskEnum {
        TASK_1_0_001("新技术-XJS_CONTENT_MANAGEMENT-外网同步数据到内网", "1-0-001"),
        TASK_0_0_001("新技术-XJS_CONTENT_MANAGEMENT-内网数据处理", "0-0-001");
        // 成员变量
        private String name;
        private String code;

        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }

        // 构造方法
        private TaskEnum(String name, String code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getNameByCode(String code) {
            for (TaskEnum c : TaskEnum.values()) {
                if (code.equals(c.getCode())) {
                    return c.name;
                }
            }
            return null;
        }
    }

    public enum OperationEnum {

        Add("增加", "a"),
        Delete("删除", "d"),
        Update("修改", "u"),
        Select("查询", "s"),
        Login("登录", "l"),
        LoginOut("退出", "o"),
        Import("导入", "i"),
        Backup("备份", "b"),
        Audit("审核", "r"),
        OutPower("越权", "op"),
        CapacityWarn("容量警告", "cw"),
        ORDERED("排序", "or"),
        STATISTICS("统计", "sc"),
        Export("导出", "e");

        // 成员变量
        private String name;
        private String code;

        // 构造方法
        private OperationEnum(String name, String code) {
            this.name = name;
            this.code = code;
        }

        // 普通方法
        public static String getNameByCode(String code) {
            for (OperationEnum c : OperationEnum.values()) {
                if (code.equals(c.getCode())) {
                    return c.name;
                }
            }
            return null;
        }

        // get set 方法
        public String getName() {
            return name;
        }

        public String getCode() {
            return code;
        }
    }

    @Getter
    @AllArgsConstructor
    public enum SystemNoticeEnum {
        WEATHER_DESC("weather_desc", "天气描述"),
        TEMPERATURE_DESC("temperature_desc", "温度描述");

        private String value;
        private String name;
    }

    @Getter
    @AllArgsConstructor
    public enum BindStatusEnum {
        WAIT_BIND("0", "待绑定"),
        WAIT_AUDIT("1", "待审核"),
        AUDITED("2", "审核通过"),
        WAIT_UNBIND("3", "待解绑"),
        UNBOUND("4", "解绑通过"),
        REJECT_UNBINDING("5", "解绑驳回");

        private String value;
        private String name;
    }
}

