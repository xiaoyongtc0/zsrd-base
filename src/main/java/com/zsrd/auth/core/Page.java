package com.zsrd.auth.core;

import java.io.Serializable;

/**
 * <pre>
 *
 * </pre>
 *
 * @author sunjiyong
 * @since 2023/9/19 16:21
 */
public class Page implements Serializable {

    /**
     * 每页显示记录数
     */
    protected int showCount;

    /**
     * 总页数
     */
    protected int totalPage;
    /**
     * 总记录数
     */
    protected int totalResult;
    /**
     * 当前页
     */
    protected int currentPage;
    /**
     * 当前记录起始索引
     */
    protected int currentResult;

    /**
     * true:需要分页的地方，传入的参数就是Page实体；false:需要分页的地方，传入的参数所代表的实体拥有Page属性
     */
    private boolean entityOrField;


    private static final long serialVersionUID = 1L;

    public Page() {
    }

    /**
     * 计算总页数
     * @return
     */
    public int getTotalPage() {
        if (totalResult % getShowCount() == 0) {
            totalPage = totalResult / getShowCount();
        }else {
            totalPage = totalResult / getShowCount() + 1;
        }
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }

    /**
     * 计算当前页码
     * @return
     */
    public int getCurrentPage() {
        if (currentPage <= 0){
            currentPage = 1;
        }
        if (currentPage > getTotalPage()) {
            currentPage = getTotalPage();
        }
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getShowCount() {
        if(showCount == 0){
            return 10;
        }
        return showCount;
    }

    public void setShowCount(int showCount) {

        this.showCount = showCount;
    }

    /**
     * 计算起始条数
     * @return
     */
    public int getCurrentResult() {
        currentResult = (getCurrentPage() - 1) * getShowCount();
        if (currentResult < 0) {
            currentResult = 0;
        }
        return currentResult;
    }

    public void setCurrentResult(int currentResult) {
        this.currentResult = currentResult;
    }

    public boolean isEntityOrField() {
        return entityOrField;
    }

    public void setEntityOrField(boolean entityOrField) {
        this.entityOrField = entityOrField;
    }
}
