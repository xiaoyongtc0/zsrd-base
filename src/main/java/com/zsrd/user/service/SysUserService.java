package com.zsrd.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.user.domain.SysUser;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 分页
     * @param
     * @return
     */
    IPage<SysUser> list(SysUser sysUser);


    /**
     * 根据id获取用户绑定的角色
     * @param userId
     * @return
     */
    SysUser getUserIdByData();



}
