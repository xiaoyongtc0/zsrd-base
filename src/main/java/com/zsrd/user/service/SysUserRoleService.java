package com.zsrd.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.system.domain.dto.UserMenu;
import com.zsrd.user.domain.SysUserRole;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
public interface SysUserRoleService extends IService<SysUserRole> {


    List<UserMenu> getRole(Object userId);
}
