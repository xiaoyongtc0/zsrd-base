package com.zsrd.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.user.domain.SysDept;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-23
 */
public interface SysDeptService extends IService<SysDept> {

    /**
     * 根据名字查寻(树)
     * @param sysDept
     * @return
     */
    List<SysDept> listByNameOrStatus(SysDept sysDept);


}
