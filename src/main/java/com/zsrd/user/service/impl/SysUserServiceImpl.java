package com.zsrd.user.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.auth.core.ResponseSupport;
import com.zsrd.auth.core.UserInfoUtil;
import com.zsrd.user.mapper.SysUserMapper;
import com.zsrd.user.domain.SysUser;
import com.zsrd.user.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public IPage<SysUser> list(SysUser sysUser) {
        Page<SysUser> page = new Page<>();
        page.setCurrent(sysUser.getPageNum());//0
        page.setSize(sysUser.getPageSize());//10
        IPage<SysUser> list = sysUserMapper.list( page,sysUser);
        return list;
    }

    @Override
    public SysUser getUserIdByData() {


        Map<String, Object> userinfo = UserInfoUtil.userinfo();
        Object userId = userinfo.get("userId");
        SysUser userIdByData = baseMapper.getUserIdByData((Long) userId);


        return userIdByData;
    }



}
