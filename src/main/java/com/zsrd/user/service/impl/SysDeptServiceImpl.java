package com.zsrd.user.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.user.domain.SysDept;
import com.zsrd.user.mapper.SysDeptMapper;
import com.zsrd.user.service.SysDeptService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-23
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {


    /**
     * 处理传过来的数据并且进行循环set进他的子集(树)
     * @param sysAreaCodes
     * @param item
     * @return
     */
    private List<SysDept> getLeafByName (List < SysDept > sysAreaCodes, SysDept item){
        Stream<SysDept> adminSysMenuStream = sysAreaCodes.stream()
                .filter(items -> items.getParentDeptId().equals(item.getDeptId()))
                .filter(items -> items.getDelFlag().equals(0))
                ;
        //如果相等就进入里面 将这条数据set进去
        List<SysDept> collect = adminSysMenuStream.map(adminSysMenu -> {
            adminSysMenu.setChildren(getLeafByName(sysAreaCodes, adminSysMenu));
            //然后返回 一直到没有了以后才返回一次list
            return adminSysMenu;
        }).collect(Collectors.toList());

        return collect;
    }


    /**
     * 处理树结构信息
     * 1.递归循环处理数据并返回一个新的list
     * 2.判断sql查寻出来的数据和递归循环后的数据进行判断 拿出重复的数据
     * 3.删除多余的数据(例如--回显数据集children已经有了这个数据,那么要将外层的这条数据删除 否则会数据重复,冗余)
     * @param sysDept
     * @return
     */
    @Override
    public List<SysDept> listByNameOrStatus(SysDept sysDept) {
        List<SysDept> sysDepts = baseMapper.listByNameOrStatus(sysDept);
        //collect1里面的数据重新递归
        Stream<SysDept> sysDeptStream = sysDepts.stream().filter(item -> item.getDelFlag().equals(0));
        //返回一个stream流方式通过.map方法将list里面的值赋值进去然后返回一个新的list集合
        List<SysDept> collect2 = sysDeptStream.map(adminSysMenu -> {
            //将上面list查寻的数据调用下面递归的方法依次进行 从0开始 到结束 才退出返回一个新的list
            adminSysMenu.setChildren(getLeafByName(sysDepts, adminSysMenu));
            return adminSysMenu;
        }).collect(Collectors.toList());
        //查看这个集合里相同的元素
        List<SysDept> collect1 = Stream.concat(collect2.stream(), sysDepts.stream())
                .distinct()
                .collect(Collectors.toList());
        //删除多余元素
        for (int i = 0; i < collect1.size(); i++) {
            SysDept sysDept2 = collect1.get(i);
            List<SysDept> children = sysDept2.getChildren();
            for (SysDept child : children) {
                collect2.remove(child);
            }
        }
        return collect2;
    }
}
