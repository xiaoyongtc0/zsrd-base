package com.zsrd.user.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.system.domain.dto.UserMenu;
import com.zsrd.user.domain.SysUserRole;
import com.zsrd.user.mapper.SysUserRoleMapper;
import com.zsrd.user.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;




    @Override
    public  List<UserMenu> getRole(Object userId) {
        return sysUserRoleMapper.getRole(userId);

    }
}
