package com.zsrd.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zsrd.user.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 分页
     * @param page
     * @param sysUser
     * @return
     */
    IPage<SysUser> list(Page<SysUser> page, @Param("sysUser") SysUser sysUser);



    SysUser getByUsername(String username);


    SysUser getUserIdByData(Long userId);


    /**
     * 获取角色绑定
     * @param userId
     * @return
     */
    List<String> getListUserById(Long userId);

}
