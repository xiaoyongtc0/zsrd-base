package com.zsrd.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsrd.system.domain.dto.UserMenu;
import com.zsrd.user.domain.SysUserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    List<SysUserRole> getAll(@Param("userId") Object userId);


    List<UserMenu> getRole(@Param("userId") Object userId);
}
