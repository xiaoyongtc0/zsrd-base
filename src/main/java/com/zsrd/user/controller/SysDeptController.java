package com.zsrd.user.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zsrd.auth.core.ResponseSupport;
import com.zsrd.user.domain.SysDept;
import com.zsrd.user.service.SysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxx
 * @since 2023-11-23
 */
@RestController
@RequestMapping("/sysDept")
@Api(value="部门列表",tags="部门列表")
public class SysDeptController {

    @Autowired
    private SysDeptService sysDeptService;


    /**
     * 根据条件查询树结构
     * 如果参数为空就查全部
     * 如果参数有值就根据参数去查寻
     * @param
     * @return
     */
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ResponseSupport getTree(@RequestBody SysDept sysDept) {
        List<SysDept> list = sysDeptService.listByNameOrStatus(sysDept);

        return ResponseSupport.success(list);

    }


    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "delete",method = RequestMethod.POST)
    @ApiOperation(value = "删除",notes = "删除")
    public ResponseSupport delete(@RequestBody SysDept sysDept){
        sysDept.setDelFlag(2);
        sysDept.setUpdateTime(new Date());
        boolean b = sysDeptService.updateById(sysDept);

        if (b==false){
            return ResponseSupport.fail("删除失败");
        }

        return ResponseSupport.success(b);
    }



    @RequestMapping(value = "save",method = RequestMethod.POST)
    @ApiOperation(value = "新增",notes = "新增")
    public ResponseSupport saveOrUpdate(@RequestBody SysDept sysDept){
             sysDept.setDelFlag(0);
             if (sysDept.getDeptId()==null) {
                 if (sysDept.getParentDeptId() == 0L) {
                     sysDept.setCreateTime(new Date());
                     sysDept.setUpdateTime(new Date());
                     sysDept.setParentDeptId(0L);
                     sysDeptService.save(sysDept);
                 } else {
                     sysDept.setCreateTime(new Date());
                     sysDept.setUpdateTime(new Date());
                     sysDept.setParentDeptId(sysDept.getParentDeptId());
                     sysDeptService.save(sysDept);
                 }
             }else {
                 sysDept.setUpdateTime(new Date());
                 sysDept.setCreateTime(new Date());
                 boolean b = sysDeptService.saveOrUpdate(sysDept);
                 if (b==false){
                     return ResponseSupport.fail("修改失败");
                 }
                 return ResponseSupport.success(b);
             }
        return ResponseSupport.success();
    }







}

