package com.zsrd.user.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.zsrd.auth.core.ResponseSupport;
import com.zsrd.auth.core.UserInfoUtil;
import com.zsrd.user.domain.SysUserRole;
import com.zsrd.user.domain.SysUser;
import com.zsrd.user.service.SysUserRoleService;
import com.zsrd.user.service.SysUserService;
import com.zsrd.user.util.RandomPassword;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@RestController
@RequestMapping("/sysUser")
@Api(value="用户列表",tags="用户列表")
public class SysUserController {

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private SysUserRoleService sysUserRoleService;
    /**
     * 分页
     * @param
     * @return
     */

    @RequestMapping(value = "list",method = RequestMethod.POST)
    @ApiOperation(value = "用户列表",notes = "用户列表")
    public ResponseSupport list(@RequestBody SysUser sysUser){

            IPage<SysUser> pbPublicityIPage = sysUserService.list(sysUser);
            return ResponseSupport.success(pbPublicityIPage);
    }

    /**
     * 查看
     * @param
     * @return
     */
    @RequestMapping(value = "getById",method = RequestMethod.POST)
    @ApiOperation(value = "查看",notes = "查看")
    public ResponseSupport getById(@RequestBody SysUser sysUser){
        SysUser byId = sysUserService.getById(sysUser);
            return ResponseSupport.success(byId);
    }

    /**
     * 删除
     * @return
     */
    @RequestMapping(value = "delete",method = RequestMethod.POST)
    @ApiOperation(value = "删除",notes = "删除")
    public ResponseSupport delete(@RequestBody SysUser sysUser){
        sysUser.setDelFlag(2);
        boolean b = sysUserService.removeById(sysUser.getUserId());
        if (b==false){
            return ResponseSupport.fail("删除失败");
        }
        return ResponseSupport.success(b);
    }
    @RequestMapping(value = "randomPassword",method = RequestMethod.POST)
    @ApiOperation(value = "随机密码生成",notes = "随机密码生成")
    public ResponseSupport randomPassword(){

        String password = RandomPassword.generateShortUUID();
        if (StringUtils.isEmpty(password)){
            return ResponseSupport.fail("密码生成为空");
        }
        return ResponseSupport.success(password);
    }

//    @Autowired
//    private BCryptPasswordEncoder encoders;

//    @Autowired
//    private PasswordEncoder passwordEncoder;

//    @RequestMapping(value = "update",method = RequestMethod.GET)
//    @ApiOperation(value = "修改",notes = "修改")
//    public ResponseSupport update(@RequestBody SysUsers sysUser){
//
//        if (!StringUtils.isEmpty(sysUser.getEncryptedPassword())) {
//
////            String hashPass = passwordEncoder.encode(sysUser.getPassword());
//
//            String encode1 = encoders.encode(sysUser.getEncryptedPassword());
//            sysUser.setPassword(encode1);
//        }
//        boolean b = sysUserService.updateById(sysUser);
//        return ResponseSupport.success(b);
//    }



    @RequestMapping(value = "update",method = RequestMethod.POST)
    @ApiOperation(value = "修改",notes = "修改")
    public ResponseSupport update(@RequestBody SysUser sysUser){

        if (!StringUtils.isEmpty(sysUser.getData())) {
            LambdaQueryWrapper<SysUserRole> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(SysUserRole::getUserId, sysUser.getUserId());
            sysUserRoleService.remove(lambdaQueryWrapper);

            String data = sysUser.getData();
            String substring = data.substring(0, data.length()-0);
            String[] split = substring.split(",");

            SysUserRole sysUserRole = new SysUserRole();
            for (String s : split) {
                sysUserRole.setUserId(sysUser.getUserId());
                sysUserRole.setRoleId(Long.valueOf(s).longValue());
                sysUserRoleService.save(sysUserRole);
            }
        }
        sysUserService.updateById(sysUser);
        return ResponseSupport.success();
    }


//    @RequestMapping(value = "useBoundRoles",method = RequestMethod.POST)
//    @ApiOperation(value = "用户获取角色",notes = "用户获取角色")
//    public ResponseSupport useBoundRoles(){
//
//        Map<String, Object> userinfo = UserInfoUtil.userinfo();
//        Object userId = userinfo.get("userId");
//        SysUser userIdByData = sysUserService.getUserIdByData((Long) userId);
//        return ResponseSupport.success(userIdByData);
//    }




}

