package com.zsrd.user.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author Xxx
 * @since 2023-11-23
 */
@Data
  @EqualsAndHashCode(callSuper = false)
@ApiModel(description = "部门实体类")
    public class SysDept implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 部门id
     */
      @ApiModelProperty("部门id")
      @TableId
      @JsonFormat(shape =JsonFormat.Shape.STRING)
      private Long deptId;

      /**
     * 部门名称
     */
      @ApiModelProperty("部门名称")
      private String deptName;

      /**
     * 部门id
     */
      @ApiModelProperty("部门父id")
      @JsonFormat(shape =JsonFormat.Shape.STRING)
      private Long parentDeptId;

      /**
     * 显示顺序
     */
      @ApiModelProperty("显示顺序")
      private Integer orderNum;

      /**
     * 负责人
     */
      @ApiModelProperty("负责人")
      private String leader;

      /**
     * 联系电话
     */
      @ApiModelProperty("联系电话")
      private String phone;


    private String mpath;

      /**
     * 部门状态（0正常 1停用）
     */
      @ApiModelProperty("部门状态（0正常 1停用）")
      private String status;

      /**
     * 创建时间
     */
        @TableField(fill = FieldFill.INSERT)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        @ApiModelProperty("创建时间")
        private Date createTime;

      /**
     * 更新时间
     */
        @TableField(fill = FieldFill.INSERT_UPDATE)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        @ApiModelProperty("更新时间")
        private Date updateTime;

      /**
     * 删除标志（0代表存在 2代表删除）
     */
      @ApiModelProperty("删除标志（0代表存在 2代表删除）")
      private Integer delFlag;


  /**
   * 树结构的下级
   */
  @TableField(exist = false)
  @ApiModelProperty("树结构的下级")
  public List<SysDept> children;

  @TableField(exist = false)
  @ApiModelProperty("分页起始")
  private Integer pageNum;

  @TableField(exist = false)
  @ApiModelProperty("分页结束")
  private Integer pageSize;


}
