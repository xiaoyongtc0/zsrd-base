package com.zsrd.user.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@Data
  @EqualsAndHashCode(callSuper = false)
@ApiModel(description = "用户和角色中间表")
    public class SysUserRole implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty("关联用户id")
    @TableId(type = IdType.AUTO)
    private Long userId;

    @ApiModelProperty("关联角色id")
    private Long roleId;

    @TableField(exist = false)
    @ApiModelProperty("字符串(1,2,3,4,5)")
    private String data;

}
