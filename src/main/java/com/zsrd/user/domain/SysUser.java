package com.zsrd.user.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author Xxx
 * @since 2023-11-22
 */
@Data
  @EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
@ApiModel(description = "用户表")
    public class SysUser implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 用户id
     */
      @ApiModelProperty("用户id")
      @TableId(value = "user_id", type = IdType.AUTO)
      private Long userId;

      /**
     * 用户账号
     */
      @ApiModelProperty("用户账号")
      private String username;

      /**
     * 密码
     */
      @ApiModelProperty("密码")
      private String password;

      /**
     * 用户昵称
     */
      @ApiModelProperty("用户昵称")
      private String nickName;

      /**
     * 部门id
     */
      @ApiModelProperty("部门id")
      private Long deptId;

      /**
     * 状态
     */
      @ApiModelProperty("状态")
      private Integer status;

      /**
     * 性别
     */
      @ApiModelProperty("性别")
      private String gender;

      /**
     * 盐加密
     */
      @ApiModelProperty("盐加密")
      private String salt;

      /**
     * 备注
     */
      @ApiModelProperty("备注")
      private String remark;

      /**
     * 创建时间
     */
        @TableField(fill = FieldFill.INSERT)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
        @ApiModelProperty("创建时间")
        private Date createTime;

      /**
     * 更新时间
     */
        @TableField(fill = FieldFill.INSERT_UPDATE)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
        @ApiModelProperty("更新时间")
        private Date updateTime;

      /**
     * 删除标志（0代表存在 2代表删除）
     */
      @ApiModelProperty("删除标志（0代表存在 2代表删除）")
      private Integer delFlag;


      @TableField(exist = false)
      @ApiModelProperty("分页起始")
      private Integer pageNum;

      @TableField(exist = false)
      @ApiModelProperty("分页结束")
      private Integer pageSize;

  /**
   * 加密密码
   */
  @TableField(exist = false)
  @ApiModelProperty("加密密码")
  private String  encryptedPassword;

  @TableField(exist = false)
  @ApiModelProperty("部门名字")
  private String  deptName;

  @TableField(exist = false)
  @ApiModelProperty("角色名字")
  private String  roleName;

  @TableField(exist = false)
  @ApiModelProperty("字符串(1,2,3,4,5,6)")
  private String  data;



}
