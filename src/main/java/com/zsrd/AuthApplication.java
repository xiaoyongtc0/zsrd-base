package com.zsrd;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * @Author LY
 * @ClassName AuthApplication
 * @Date 2023/6/10 14:26
 * @Description 启动类
 * @Version 1.0
 */

@MapperScan("com.zsrd.*.mapper")
@Slf4j
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class AuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class,args);
    }
}
