package com.zsrd.system.aspect.enums;

/**
 * 操作人类别
 */
public enum FunctionType {
    /**
     * 其他
     */
    OTHER,
    /**
     * 超级表格
     */
    PROTABLE,
    /**
     * 菜单管理
     */
    MENUMANAGE
}
