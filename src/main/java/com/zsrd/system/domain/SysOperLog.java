package com.zsrd.system.domain;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class SysOperLog {

  @TableId
  private long operId;
  private String title;
  private long businessType;
  private String method;
  private String requestMethod;
  private long operatorType;
  private String operName;
  private String deptName;
  private String operUrl;
  private String operIp;
  private String operLocation;
  private String operParam;
  private String jsonResult;
  private long status;
  private String errorMsg;
  private Date operTime;
  private String functionType;

}
