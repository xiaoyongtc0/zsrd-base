package com.zsrd.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysMenu implements Serializable {

  @TableId("menu_id")
  private long menuId;

  private long parentMenuId;
  private String name;
  private String menuName;
  private String path;
  private long orderNum;
  private String component;
  private String mpath;
  private String query;
  private String linkUrl;
  private String icon;
  private long isFrame;
  private long isKeepAlive;
  private long isFull;
  private long isHide;

  @TableField(fill = FieldFill.INSERT)
  private Date createTime;

  @TableField(fill = FieldFill.INSERT_UPDATE)
  private Date updateTime;

  @ApiModelProperty("删除标志（0代表存在 2代表删除）")
  private Integer delFlag;

  /**
   * 树结构的下级
   */
  @TableField(exist = false)
  @ApiModelProperty("树结构的下级")
  private List<SysMenu> item;

  @TableField(exist = false)
  private Map meta;

  @TableField(exist = false)
  @ApiModelProperty("分页起始")
  private Integer pageNum;

  @TableField(exist = false)
  @ApiModelProperty("分页结束")
  private Integer pageSize;

}
