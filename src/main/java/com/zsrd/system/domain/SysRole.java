package com.zsrd.system.domain;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRole implements Serializable {

  private static final long serialVersionUID = 1L;

  @TableId("role_id")
  private long roleId;

  private String roleName;

  private String roleKey;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date createTime;

  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private Date updateTime;

  private String delFlag;

  @TableField(exist = false)
  private List<SysMenu> SysMenuList;

  @TableField(exist = false)
  private String menuId;

  @TableField(exist = false)
  @ApiModelProperty("分页起始")
  private Integer pageNum;

  @TableField(exist = false)
  @ApiModelProperty("分页结束")
  private Integer pageSize;
}
