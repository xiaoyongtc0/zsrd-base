package com.zsrd.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserMenu {

    private long menuId;
    private long parentMenuId;
    private String name;
    private String menuName;
    private String path;
    private long orderNum;
    private String component;
    private String mpath;
    private String query;
    private String linkUrl;
    private String icon;
    private long isFrame;
    private long isKeepAlive;
    private long isFull;
    private long isHide;
    private Date createTime;
    private Date updateTime;

    private Map meta;
    private String UserId;

}
