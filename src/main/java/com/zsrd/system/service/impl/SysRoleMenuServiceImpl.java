package com.zsrd.system.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.system.domain.SysRoleMenu;
import com.zsrd.system.mapper.SysRoleMenuMapper;
import com.zsrd.system.service.SysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Autowired
     private  SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public List<SysRoleMenu> getAll(long roleId) {
       return sysRoleMenuMapper.getAll(roleId);

    }
}