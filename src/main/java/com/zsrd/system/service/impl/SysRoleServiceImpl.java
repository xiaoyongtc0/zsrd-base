package com.zsrd.system.service.impl;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.auth.core.ResponseSupport;
import com.zsrd.system.domain.SysMenu;
import com.zsrd.system.domain.SysRole;
import com.zsrd.system.domain.SysRoleMenu;
import com.zsrd.system.mapper.SysRoleMapper;
import com.zsrd.system.service.SysRoleMenuService;
import com.zsrd.system.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Override
    public IPage<SysRole> listByPage(SysRole sysRole) {
        Page<SysRole> page = new Page<>();
        page.setCurrent(sysRole.getPageNum());//0
        page.setSize(sysRole.getPageSize());//10
        IPage<SysRole> list = baseMapper.listByPage(page,sysRole);
        return list;
    }


    @Override
    public SysRole selectByName(String roleName) {
      return   baseMapper.selectByName(roleName);

    }

    @Override
    @Transactional
    public Boolean updateMenu(SysRole sysRole) {
        long roleId = sysRole.getRoleId();
        String id = String.valueOf(roleId);
        if (StrUtil.equals(id, "0")) {
            sysRole.setCreateTime(new Date());
            sysRole.setUpdateTime(new Date());
            // 新增用户
            save(sysRole);
            // 新增关联表信息
            String[] split = sysRole.getMenuId().split(",");
            List<SysRoleMenu> list = new ArrayList<>();
            for (String s : split) {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setMenuId(s);
                // 去数据库里面通过name找对应的id
                SysRole sysRole1 = selectByName(sysRole.getRoleName());
                sysRoleMenu.setRoleId( sysRole1.getRoleId());
                list.add(sysRoleMenu);
            }
            sysRoleMenuService.saveBatch(list);
        } else {
            SysRole byId = getById(id);
            if (byId == null) {
                return false;
            }

            // 修改角色信息
            sysRole.setUpdateTime(new Date());
            updateById(sysRole);
            // 修改中间表的对应关系 先删除
            LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(SysRoleMenu::getRoleId, sysRole.getRoleId());
            sysRoleMenuService.remove(wrapper);
            String[] split = sysRole.getMenuId().split(",");
            List<SysRoleMenu> list = new ArrayList<>();
            for (String s : split) {
                SysRoleMenu sysRoleMenu = new SysRoleMenu();
                sysRoleMenu.setMenuId(s);
                sysRoleMenu.setRoleId(sysRole.getRoleId());
                list.add(sysRoleMenu);
            }
            sysRoleMenuService.saveBatch(list);
            return true;
        }
        return null;
    }
}