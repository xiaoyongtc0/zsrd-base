package com.zsrd.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zsrd.system.domain.SysMenu;
import com.zsrd.system.mapper.SysMenuMapper;
import com.zsrd.system.service.SysMenuService;
import org.springframework.stereotype.Service;


@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {


    @Override
    public IPage<SysMenu> listByPage(SysMenu sysMenu) {
        Page<SysMenu> page = new Page<>();
        page.setCurrent(sysMenu.getPageNum());//0
        page.setSize(sysMenu.getPageSize());//10
        IPage<SysMenu> list = baseMapper.listByPage(page,sysMenu);
        return list;

    }


}
