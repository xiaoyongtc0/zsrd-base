package com.zsrd.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.system.domain.SysRole;


public interface SysRoleService extends IService<SysRole> {


    IPage<SysRole> listByPage(SysRole sysRole);

    SysRole selectByName(String roleName);

    Boolean updateMenu(SysRole sysRole);
}
