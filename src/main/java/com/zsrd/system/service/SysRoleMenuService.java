package com.zsrd.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.system.domain.SysRoleMenu;

import java.util.List;


public interface SysRoleMenuService extends IService<SysRoleMenu> {


    List<SysRoleMenu> getAll(long roleId);
}
