package com.zsrd.system.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zsrd.system.domain.SysMenu;

public interface SysMenuService extends IService<SysMenu> {

    IPage<SysMenu> listByPage(SysMenu sysMenu);


}
