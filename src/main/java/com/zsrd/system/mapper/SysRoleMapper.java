package com.zsrd.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zsrd.system.domain.SysMenu;
import com.zsrd.system.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    void updateRole(@Param("sysRole") SysRole sysRole);

    void updateRoleMenu(@Param("sysRole") SysRole sysRole);

    IPage<SysRole> listByPage(@Param("page") Page<SysRole> page, @Param("sysRole") SysRole sysRole);

    SysRole selectByName(@Param("roleName") String roleName);
}
