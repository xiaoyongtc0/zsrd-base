package com.zsrd.system.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zsrd.system.domain.SysMenu;
import com.zsrd.system.domain.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    IPage<SysMenu> list(@Param("page") Page<SysMenu> page, @Param("sysRole") SysRole sysRole);

    List<SysMenu> selectAll();

    IPage<SysMenu> listByPage(@Param("page") Page<SysMenu> page, @Param("sysMenu") SysMenu sysMenu);

}
