package com.zsrd.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zsrd.system.domain.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    List<SysRoleMenu> getAll(@Param("roleId") long roleId);
}
