package com.zsrd.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zsrd.system.domain.SysOperLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {



}
